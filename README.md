Tasks to answer in your own README.md that you submit on Canvas:

 1.  See logger.log, why is it different from the log to console?
		The logger.properties file has Console Logging set to INFO while the File Logging is set to ALL.
 2.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']
		The line is printed in the logger.log file. It originates from the ConditionEvaluator class in the org.junit.jupiter.engine.execution library.
 3.  What does Assertions.assertThrows do?
		Asserts that execution of the supplied executable throws an exception of the expectedType and returns the exception. If no exception is thrown, or an exception of a different type is thrown, this method will fail.
 4.  See TimerException and there are 3 questions
     1.  What is serialVersionUID and why do we need it? (please read on Internet)
			It is a serial version number that ensures that the serialized version of the object is the same as the serialized version of the class. If they are different, an InvalidClassException is thrown.
     2.  Why do we need to override constructors?
			Overriding the default constructor is not enough. In order to define our own TimerException, then all possible constructors of the Exception class must be overridden to be able to create our new TimerException.
     3.  Why we did not override other Exception methods?	
			The other Exception methods can be called by our new TimerException because our TimerException extends Exception and therefore inherits its methods.
 5.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
		The static {} block tries to open the "logger.properties" file to read its configuration. If there is an IOException caught, some warnings are output to the console. Finally, the message "starting the app" is logged.
 6.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
		If your repository contains a README.md file at the root level, Bitbucket Server displays its contents on the repository's Overview page if the file has the .md extension. The file can contain Markdown and a restricted set of HTML tags.
 7.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
		If the timeMe function reached the finally block, the timeNow variable would still be set to null. Replace timeNow = null with timeNow = System.currentTimeMillis();
 8.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
		A TimerException is thrown when the timeToWait is -1 because timeToWait cannot be less than 0. A NullPointerException is also thrown.
 9.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
10.  Make a printScreen of your eclipse Maven test run, with console
11.  What category of Exceptions is TimerException and what is NullPointerException
		TimerException is a compile time Exception and NullPointerException is a runtime Exception.
12.  Push the updated/fixed source code to your own repository. 